package ru.pae.bank;

public class Main {
    public static void main(String[] args) {
        Bank b1 = new Bank(15000, 12.5);
        int year = 365;
        double rate = b1.getBalance() + b1.getBalance() * b1.getPercent() / year / 100;
        System.out.println(rate);
    }
}
