package ru.pae.bank;

public class Bank {
    private double balance;
    private double percent;

    Bank(double balance, double percent) {
        this.balance = balance;
        this.percent = percent;
    }

    double getBalance() {
        return balance;
    }

    double getPercent() {
        return percent;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "Балланс=" + balance +
                ", Процент=" + percent +
                '}';
    }
}

